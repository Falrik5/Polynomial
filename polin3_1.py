from itertools import combinations
from itertools import product
import math

'''
Функция Conjunctions принимает на вход один аргумент - number_variables
number_variables - список, с элементами типа int - номерами БП
функция возвращает все возможные элементарные конъюнкции (в том числе и конъюнкцию 1) в виде списка
'''
def Conjunctions(number_variables):
    n = len(number_variables)
    mass = []
    mass += ["1"]                                                 
    boolvar = []
    for i in number_variables:
        boolvar += [[("-x" + str(i)), ("x" + str(i))]]
    for var in boolvar:
        mass += [var[0]]
        mass += [var[1]]
    for i in range(2, n + 1):
        comb_neg = list(product(range(2), repeat=i))
        comb_var = list(combinations(boolvar, i))
        for next in comb_var: 
            for neg in comb_neg:
                new_con = ""
                for j in range(i - 1):
                    new_con += str(next[j][neg[j]]) + " & "
                new_con += str(next[i - 1][neg[i - 1]])
                mass += [new_con]
    return mass

'''
Функция SetsVectors принимает на вход один аргумент - n
n - целое число, количество БП
Функция возвращает список наборов длины n, упорядоченный по возрастанию
'''
def SetsVectors(n):
    all_sets = list(product(range(2), repeat=n))
    sets_vector = []
    for s in all_sets:
        func = ""
        for i in range(n):
            func += str(s[i])
        sets_vector += [func]
    return sets_vector

'''
Функция FuncVectors принимает на вход один аргумент - n
n - целое число, количество БП
Функция возвращает список функций, зависящих от n БП, упорядоченный по возрастанию
'''
def FuncVectors(n):
    all_func = list(product(range(2), repeat=2**n))
    functionals = []
    for f in all_func:
        func = ""
        for i in range(2**n):
            func += str(f[i])
        functionals += [func]
    return functionals

'''
Функция Enumeration принимает на вход два аргумента - vector_func, variables
vector_func - вектор значений функции, для которой будет строится полином
variables - список номеров переменных, от которых зависит функция, вектор значений которой равен vector_func
Функция возвращает с помощью перебора всех возможных полиномов находит полином минимальной длины, реализующий функцию с вектором значений vector_func
Возвращает список, состоящий из 2 элементов:
1 элемент - искомый полином в виде строки
2 элемент - длина искомого полинома
'''
def Enumeration(vector_func, variables):
    n = int(math.log(len(vector_func), 2)) # Количество переменных
    all_func = FuncVectors(n)
    elementary_conjunctions = Conjunctions(variables) # Все ЭК от данных переменных
    minimal_polyn = {all_func[i]: ["-", 1000] for i in range(2**2**n)} # Словарь, где каждой функции от переменных с номерами variables сопоставлен список из 2 элементов

    # Сразу обработаем случай нулевой функции
    minimal_polyn[all_func[0]][0] = "0"
    minimal_polyn[all_func[0]][1] = 0

    # Будем перебирать полиномы по мере возрастания длины, пока не найдем первый полином, реализующий функцию с вектором значений vector_func
    sets = list(product(range(2), repeat=n))
    len_p = 1
    while (minimal_polyn[vector_func] == ["-", 1000]):
        part = list(combinations(elementary_conjunctions, len_p))
        conjunctions = [""] * len(part)
        for i in range(len_p - 1):
            for j in range(len(part)):
                conjunctions[j] += str(part[j][i]) + " ^ "
        for i in range(len(conjunctions)):
            conjunctions[i] += part[i][len_p - 1]
        for c in conjunctions:
            func = "" 
            new_polyn = c
            new_polyn = new_polyn.replace(" ", "")
            for set in sets:
                for i in range(n):
                    new_polyn = new_polyn.replace(str("x" + str(variables[i])), str(set[i]))
                new_polyn = new_polyn.replace("-0", "1")
                new_polyn = new_polyn.replace("-1", "0")
                value_con = int(new_polyn[0])
                value = 0
                i = 1
                while (i < len(new_polyn)):
                    if (new_polyn[i] == '&'):
                        i += 1
                        value_con *= int(new_polyn[i])
                        i += 1
                    elif (new_polyn[i] == '^'):
                        value = value ^ value_con
                        i += 1
                        value_con = int(new_polyn[i])
                        i += 1
                value = value ^ value_con
                func += str(value)
                new_polyn = c.replace(" ", "")
            if (minimal_polyn[func][1] > len_p):
                minimal_polyn[func][0] = c
                minimal_polyn[func][1] = len_p
        len_p += 1
        part.clear()
        conjunctions.clear()
    return(minimal_polyn[vector_func])

'''
Функция ConjunctionBrackets принимает на вход два аргумента - polynom, var
polynom - строка, в которую записан некоторый полином
var - строка, а которую записано имя некоторой переменной
Функция преднозначена для случая var & (polynom), чтобы расскрыть скобки и добавить в каждую ЭК полинома polynom БП var
Возвращает строку, в которой записан преобразованный полином
'''
def ConjunctionBrackets(polynom, var):
    res = ''
    conjunctions = polynom.split(' ^ ')
    for con in conjunctions:
        res += var + ' & ' + con + ' ^ '
    res = res[:len(res) - 3]
    return res

'''
Функция InputIsBin принимает на вход один аргумент - string
string - строка
Функция проверяет, состоит ли данная строка только из символов '0' и '1'
Возвращает значение типа Bool:
True - если строка состоит только из '0' и '1'
False - в противном случае
'''
def InputIsBin(string):
    for character in string:
        if character != '0' and character != '1':
            return False
    return True

'''
Рекурсивная функция ShannonDecomposition принимает на вход аргументы - var_dec, var_pol, func
var_dec - список номеров переменных
var_pol - список номеров переменных
func - вектор значений функции
ShannonDecomposition строит полином минимальной длинны, реализующий функцию с вектором значений func,
полученный с помощью разложения Шеннона данной функции по переменным с номерами var_dec до трёх переменных
с номерами var_pol по которым строится минимальные полиномы с помощью функции enumeration
Возвращаемое значение - список, состоящий из 2 элементов:
1 элемент - искомый полином в виде строки
2 элемент - длина искомого полинома
'''
def ShannonDecomposition(var_dec, var_pol, func):
    len_f = len(func)
    var_number = int(math.log2(len_f)) # Количество переменных
    all_sets = SetsVectors(var_number)
    main_func = {all_sets[i]: func[i] for i in range(2**var_number)} # Словарь, в котором каждому набору в виде строки сопоставлено значение из func

    # каждой переменной присвоим индекс в соответствие с ростом её номера: 0 самому маленькому, var_number - 1 самому большому
    # раскладывать будем по первой переменной из списка var_dec, её индекс выделим отдельно
    i = 0
    h = 0
    index_other = [] # индексы всех переменных, кроме той, по которой происходит разложение Шеннона
    while (i < 3) and (var_dec[0] > var_pol[i]):
        index_other += [h]
        i += 1
        h += 1
    index_var_dec = h # индекс переменной, по которой происходит разложение Шеннона
    h += 1
    for i in range (h, var_number):
        index_other += [i]
    
    sets = list(product(range(2), repeat=var_number - 1)) # список наборов, длины var_number - 1, отсортированный по возрастанию
    polynom_func0 = '' # вектор-функция полученная из func, подставлением 0 вместо переменной, по котророй происходит разложение
    polynom_func1 = '' # вектор-функция полученная из func, подставлением 1 вместо переменной, по котророй происходит разложение
    polynom_func_g = '' # вектор-функция полученная из polynom_func0 ^ polynom_func1
    func_part_set0 = ['x' for i in range(var_number)]
    func_part_set0[index_var_dec] = '0' # Строка, в которой будут состовлятся наборы с подставлением 0 вместо переменной, по котророй происходит разложение
    func_part_set1 = ['x' for i in range(var_number)]
    func_part_set1[index_var_dec] = '1' # Строка, в которой будут состовлятся наборы с подставлением 1 вместо переменной, по котророй происходит разложение

    # Составим с помощью перебора наборов из sets вектор-функции polynom_func0 и polynom_func1 
    for s in sets:
        for i in range(var_number - 1):
            func_part_set0[index_other[i]] = str(s[i])
            func_part_set1[index_other[i]] = str(s[i])
        polynom_func0 += main_func[''.join(func_part_set0)]
        polynom_func1 += main_func[''.join(func_part_set1)]
    
    # Составим вектор-функцию polynom_func_g
    for i in range(int(len_f / 2)):
        polynom_func_g += str(int(polynom_func0[i]) ^ int(polynom_func1[i]))
    
    polynom0 = [] # полином для случая, когда подставили 0
    polynom1 = [] # полином для случая, когда подставили 1
    polynom_g = [] # полином полученный из сложения по модулю 2 polynom0 и polynom1

    if (var_number > 4): # если переменных больше 4, то переходим к следующей итерации: разложения по следующей переменной из var_dec
        polynom0 = ShannonDecomposition(var_dec[1:], var_pol, polynom_func0)
        polynom1 = ShannonDecomposition(var_dec[1:], var_pol, polynom_func1)
        polynom_g = ShannonDecomposition(var_dec[1:], var_pol, polynom_func_g)

    else: # иначе, с помощью перебора полиномов получаем полиномы от 3 оставшихся переменных
        numbers_of_var = [x + 1 for x in var_pol]
        polynom0 = Enumeration(polynom_func0, numbers_of_var)
        polynom1 = Enumeration(polynom_func1, numbers_of_var)
        polynom_g = Enumeration(polynom_func_g, numbers_of_var)
    
    # если polynom_g = 0, то polynom_func_g - нулевой вектор, значит polynom_func0 и polynom_func1 совпадают, значит переменная разложения является фиктивной для func
    if (polynom_g[0] == '0'):
        return [polynom0[0], polynom0[1]]

    # Находим минимальный полином из 3 случаев, при этом расскрываем скобки, полученные в пути разложения по Шеннону
    sh_split_len = min(polynom0[1] + polynom1[1], polynom0[1] + polynom_g[1], polynom_g[1] + polynom1[1])
    sh_split = ''
    number_var_dec = var_dec[0] + 1
    if (sh_split_len == polynom0[1] + polynom1[1]):
        if (polynom0[0] != '0'):
            sh_split += ConjunctionBrackets(polynom0[0], '-x' + str(number_var_dec)) + ' ^ '
        sh_split = sh_split.replace('-x' + str(number_var_dec) + ' & 1', '-x' + str(number_var_dec))
        if (polynom1[0] != '0'):
            sh_split += ConjunctionBrackets(polynom1[0], 'x' + str(number_var_dec))
        else:
            sh_split = sh_split[:len(sh_split) - 3]
        sh_split = sh_split.replace('x' + str(number_var_dec) + ' & 1', 'x' + str(number_var_dec))

    elif (sh_split_len == polynom0[1] + polynom_g[1]):
        sh_split += ConjunctionBrackets(polynom_g[0], 'x' + str(number_var_dec))
        sh_split = sh_split.replace('x' + str(number_var_dec) + ' & 1', 'x' + str(number_var_dec))
        sh_split += ' ^ ' + polynom0[0]

    else:
        sh_split += ConjunctionBrackets(polynom_g[0], '-x' + str(number_var_dec))
        sh_split = sh_split.replace('-x' + str(number_var_dec) + ' & 1', '-x' + str(number_var_dec))
        sh_split += ' ^ ' + polynom1[0]
    
    return [sh_split, sh_split_len]

input_data = ''
# Считываем число БП
flag = 0
while (flag == 0):
    print("Please enter the number of Boolean variables")
    print("for example: 2")
    input_data = input()
    flag = 1
    try:
        int(input_data)
    except ValueError:
        flag = 0
        print("it's not an integer")
var_number = int(input_data)
set3 = list(product(range(2), repeat=3))
all_func = FuncVectors(var_number) # Находим все функции от n БП
f = open(f'output_for_{var_number}.txt', 'w')
print(all_func[0], ':', 0, 'length', 1, file=f) # Отдельно обработаем случай, когда данная функция - константа 0
const1 = all_func[2**2**var_number - 1] # Отдельно обработаем случай, когда данная функция - константа 1
all_func = all_func[1: 2**2**var_number - 1]

if (var_number < 4): # # Если это функции не более, чем от 3 переменных, то воспользуемся простым перебором
    variables = [i + 1 for i in range(var_number)] # Составим список из номеров переменных, начиная с 1
    for func in all_func:
        polynom = Enumeration(func, variables)
        print(func, ':', polynom[0], 'length', polynom[1], file=f)

else: # Если это функция более, чем от 3 переменных, то воспользуемся разложение Шеннона
    all_sets = SetsVectors(var_number)

    # Получим все возможные наборы номеров переменных, по которым можно разложить по Шеннону данную функцию до трёх переменных (нумеруем с 0)
    shannon = list(combinations(range(var_number), var_number - 3))

    for func in all_func: 
        min_len = 10000
        res_split = ""
        main_func = {all_sets[i]: func[i] for i in range(2**var_number)} # Словарь, в котором каждому набору в виде строки сопоставлено значение из func

        # По каждому набору из shannon разложим по Шеннону данную функцию, получим полином по каждому и найдем среди них минимальный 
        for splitting in shannon:
            numbers = [i for i in range(var_number)]
            index3 = [item for item in numbers if item not in list(splitting)]
            polynom = ShannonDecomposition(splitting, index3, func)
            if (min_len > polynom[1]):
                res_split = polynom[0]
                min_len = polynom[1]

        # Вывод результата
        print(func, ':', res_split, 'length', min_len, file=f)
print(const1, ':', 1, 'length', 1, file=f)
f.close()